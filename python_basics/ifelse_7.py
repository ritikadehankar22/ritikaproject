num = 153;
a = num%10;
b = num//10;
c = b%10;
d = b//10;
cube_a = a*a*a;
cube_c = c*c*c;
cube_d = d*d*d;
sum = cube_a+cube_c+cube_d;
if(sum == num):
    print(f"Success:Sum of cube of {num} is {sum}");
else:
    print(f"Failed");
