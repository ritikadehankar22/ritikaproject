import random
random = random.randint(0,50)


print("Hard = 5 attempts");
print("Medium = 10 attempts");
print("Easy = 15 attempts");
level = input("Enter level :");

if(level == "hard"):
    attempts = 5;

elif(level == "medium"):
    attempts = 10;

elif(level == "easy"):
    attempts = 15;

else:
    print("Enter valid input");


for i in range(1,attempts+1):
    num = int(input("Guess a number between 0-50 :"));

    if(num == random):
        print("Congrats!! You won");
        break;
    elif(num < random):
        print("Enter a greater number");
    elif(num > random):
        print("Enter a smaller number");


if(i == attempts):
    print("Better luck next time.");
