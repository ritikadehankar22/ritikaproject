num1 = 45;
num2 = 55;
last1 = num1%10;
last2 = num2%10;
if(last1 == last2):
    print(f"Last digits of {num1} and {num2} are same.");
else:
    print(f"Last digits of {num1} and {num2} are not same.");
