radius = 5;
volume = 4/3 * 3.14 * radius * radius * radius;
print(f"Volume of sphere having radius {radius} is {volume}");
