employees = [ 
        {"id" : 1 , "name" : "ritika" , "city" : "nagpur" , "salary" : 10000},
        {"id" : 2 , "name" : "raunak" , "city" : "gondia" , "salary" : 10000},
        {"id" : 3 , "name" : "animesh" , "city" : "gujrat" , "salary" : 10000},
        {"id" : 4 , "name" : "shivam" , "city" : "ahemadabad" , "salary" : 70000},
        {"id" : 5 , "name" : "sakshi" , "city" : "raipur" , "salary" : 10000},
        {"id" : 6 , "name" : "tanmay" , "city" : "pune" , "salary" : 30000},
        {"id" : 7 , "name" : "apoorva" , "city" : "delhi" , "salary" : 10000},
        {"id" : 8 , "name" : "ananya" , "city" : "mumbai" , "salary" : 25000},
        {"id" : 9 , "name" : "drishti" , "city" : "nasik" , "salary" : 10000},
        {"id" : 10 , "name" : "snehal" , "city" : "banglore" , "salary" : 15000}
        ]

print("DETAILS OF EMPLOYEES")
from prettytable import PrettyTable
table = PrettyTable(["id","name","city","salary","tax"]);
for i in range(0,len(employees)):
    tax = employees[i]["salary"] * 10//100
    table.add_row([employees[i]["id"],employees[i]["name"],employees[i]["city"],employees[i]["salary"],tax]);
print(table);


print("SUM , AVERAGE , MAXIMUM AND MINIMUM SALARY")
sum = 0;
avg = 0;
max = employees[0]["salary"];
min = employees[0]["salary"];

for i in range(0,len(employees)):
    sum = sum + employees[i]["salary"];
    avg = sum//len(employees)
    if(max < employees[i]["salary"]):
        max = employees[i]["salary"]
    if(min > employees[i]["salary"]):
        min = employees[i]["salary"]
my_table = PrettyTable(["sum","avg","min","max"]);
my_table.add_row([sum,avg,min,max]);
print(my_table);

print("MAXIMUM SALARIED EMPLOYEES")
myy_table = PrettyTable(["id","name","city","salary"]);
for i in range(0,len(employees)):
    if(max == employees[i]["salary"]):
        myy_table.add_row([employees[i]["id"],employees[i]["name"],employees[i]["city"],employees[i]["salary"]]);
print(myy_table);

print("MINIMUM SALARIED EMPLOYEES")
myyy_table = PrettyTable(["id","name","city","salary"]);
for i in range(0,len(employees)):
    if(min == employees[i]["salary"]):
        myyy_table.add_row([employees[i]["id"],employees[i]["name"],employees[i]["city"],employees[i]["salary"]]);
print(myyy_table);



