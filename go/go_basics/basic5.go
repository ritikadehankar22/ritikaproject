package main
import "fmt"
func main() {
	var r float64
	const PI float64 = 3.14
	var volume float64

	fmt.Print("Enter radius : ")
	fmt.Scanln(&r)

	volume = 1.33 * PI * r * r * r
	fmt.Printf("Volume of sphere having radius %v is %v\n",r,volume)

}
