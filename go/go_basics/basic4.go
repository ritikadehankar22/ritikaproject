package main
import "fmt"
func main() {
	var num int16
	var a int16
	var b int16
	var c int16
	var d int16
	var e int16
	var sum int16

	fmt.Print("Enter 4 digit number : ")
	fmt.Scanln(&num)
	
	a = num/1000 //1
	b = num%10 //4
	c = (num%1000)/100 //2
	d = num%100 //34
	e = d/10 //3
	sum = a+b+c+e
	fmt.Printf("%v + %v + %v + %v = %v\n ",a,c,e,b,sum)
}
