package main
import "fmt"
func main() {
	var x int16
	var y int16

	fmt.Print("Enter value of x : ")
	fmt.Scanln(&x)

	fmt.Print("Enter value of y : ")
        fmt.Scanln(&y)
	
	x = x+y
	y = x-y
	x = x-y

	fmt.Printf("After swapping value of x is %v and value of y is %v\n",x,y)
}
