package main
import "fmt"
func main() {
	var a int16
	var b int16
	var c int16

	fmt.Print("Enter a : ")
	fmt.Scanln(&a)
	fmt.Print("Enter b : ")
	fmt.Scanln(&b)

	c = a+b
	a = c-a
	b = c-b

	fmt.Printf("After swapping value of a is %v and value of b is %v\n",a,b)
}
