package main
import "fmt"
func main() {
	var inch int16
	var foot int16
	var inches int16

	fmt.Print("Enter length in inches : ")
	fmt.Scanln(&inch)

	foot = inch/12
	inches = inch%12
	
	fmt.Printf("%v inches is equal to %v foot and %v inches\n",inch,foot,inches)
}
