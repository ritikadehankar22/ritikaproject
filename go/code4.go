package main
import "fmt"
func main() {
	var num int16
	fmt.Print("Enter number : ")
	fmt.Scan(&num)

	fmt.Println("Square of ",num,"is", num*num)
	// fmt.Println("Square of %v is %v",num,num*num)
}
