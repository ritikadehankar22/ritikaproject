package main
import "fmt"
func main() {
	var num int
	var a int
	var b int
	var c int
	var rev int

	fmt.Print("Enter a number : ")
	fmt.Scanln(&num)
	
	a = num/100
	b = (num/10)%10
	c = num%10
	rev = c*100 + b*10 + a*1

	if(num == rev) {
		fmt.Printf("%v is palindrome number\n",num)
	} else {
		fmt.Printf("%v is not palindrome number\n",num)
	}

}
