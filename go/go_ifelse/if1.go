package main
import "fmt"
func main() {
	var num int

	fmt.Print("Enter a number : ")
	fmt.Scanln(&num)

	if(num%2 == 0){
		fmt.Printf("%v is even\n",num)
	} else {
		fmt.Printf("%v is odd\n",num)
	}
}
