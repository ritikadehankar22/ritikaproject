import pymysql
import csv
from prettytable import PrettyTable
conn = pymysql.connect(host='localhost',user='diituser', password = "%TGBbgt5", db='ecom')
cur = conn.cursor(pymysql.cursors.DictCursor)

def export():
    to_csv = show();
    keys = to_csv[0].keys()
    file_path = 'employees.csv';
    with open(file_path,'w',newline='') as output_file:
        dict_writer = csv.DictWriter(output_file,keys)
        dict_writer.writeheader()
        dict_writer.writerows(to_csv)

    print(f"Data exported to {file_path} successfully")

def delete():
    user_id = int(input("enter the id whose record is to be deleted : "))
    sql = f"delete from employees where employee_id = {user_id}"
    cur.execute(sql)
    conn.commit();
    show();

def insert():
    emp_id = int(input("enter employee id : "))
    user_role = input("enter role : ")
    user_name = input("enter name : ")
    user_address = input("enter address : ")
    user_salary = int(input("enter salary : "))
    user_contact = int(input("enter contact no : "))

    sql = f"INSERT INTO `employees` (`id`, `name`, `contact_number`, `address`, `salary`, `employee_id`, `role`, `created_at`, `updated_at`) VALUES (NULL,'{user_name}', '{user_contact}', '{user_address}', '{user_salary}', '{emp_id}', '{user_role}', NULL, NULL);"
    cur.execute(sql)
    conn.commit();
    show()

def update():
    emp_id = int(input("enter employee id whose details should be changed : "))
    user_name = input("enter name : ")
    user_address = input("enter address : ")
    user_salary = int(input("enter salary : "))
    role = input("enter role of employee : ")
    user_contact = int(input("enter contact no : "))

    sql = f"UPDATE `employees` SET `name` = '{user_name}', `contact_number` = '{user_contact}', `address` = '{user_address}', `salary` = '{user_salary}', `role` = '{role}',`updated_at` = NOW() WHERE `employees`.`employee_id` = {emp_id};"
    cur.execute(sql)
    conn.commit();
    show()

def show():
    sql = f"select * from employees";
    cur.execute(sql)
    output = cur.fetchall()
    #print(output)
    table = PrettyTable(["EMPLOYEE ID","NAME","CONTACT","ADDRESS","SALARY","ROLE"])
    for employees in output:
        table.add_row([employees["employee_id"],employees["name"],employees["contact_number"],employees["address"],employees["salary"],employees["role"]])
    print(table)
    return output;

operation = None
while(operation != 0):
    print("1.SHOW\n2.DELETE\n3.ADD\n4.EDIT\n5.IMPORT\n6.EXPORT\n0.EXIT")
    operation = int(input("CHOOSE ANY ONE OPERATION : "))

    if(operation == 0):
        exit
        break;
    if(operation == 1):
        show()
    if(operation == 2):
        delete()
    if(operation == 3):
        insert()
    if(operation == 4):
        update()
    if(operation == 6):
        export()
    if(operation == 5):
        import_csv()

