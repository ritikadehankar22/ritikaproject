<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<?php

require("globalfunction.php");
session_start();

authorize();

?>

<form action="add_action.php">
	<div class="btn btn-secondary"><input type="text" autofocus name="name" placeholder="Enter name"/></div> <br/>
	<div class="btn btn-secondary"><input type="text" name="age" placeholder="Enter age"/></div> <br/>
	<div class="btn btn-secondary"><input type="text" name="city" placeholder="Enter city"/></div> <br/>
	<div class="btn btn-dark"><button type="submit"/>Submit</button></div>
</form>
