<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


<?php
require("dbconn.php");
require("globalfunction.php");

session_start();
authorize();

echo "<center><h1>Hello " . $_SESSION['username'] . "</h1></center>";
?>

<a class="btn btn-secondary" href="add_form.php">Add New Record</a>
<a class="btn btn-info" style = "float:right" href="logout.php">Logout</a>

<?php
$sql = "SELECT * FROM users order by id desc";
$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetchAll();


?>

<table class="table table-success table-striped">
        <thead>
                <th>Id</th>
                <th>Name</th>
                <th>Age</th>
                <th>City</th>
                <th>Action</th>
        </thead>
        <tbody>
                <?php foreach($records as $users) { ?>
                <tr>
                        <td><?php echo $users['id']; ?></td>
                        <td><?php echo $users['name']; ?></td>
                        <td><?php echo $users['age']; ?></td>
                        <td><?php echo $users['city']; ?></td>
                        <td>
                         <a class="btn btn-dark" href="edit_form.php?id=<?php echo $users['id']; ?>">Edit</a>

                <?php if($_SESSION['username'] == 'superadmin') { ?>
                        <a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete record of <?php echo $users['name']; ?> ?');" href="delete.php?id=<?php echo $users['id']; ?>">Delete</a>
                <?php } ?>
                        </td>

                </tr>
                <?php } ?>
        </tbody>
</table>

