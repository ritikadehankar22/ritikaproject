<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<?php

require("dbconn.php");
require("globalfunction.php");
session_start();

authorize();

$id = $_REQUEST['id'];
$sql = "SELECT * FROM users WHERE id=$id";
$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetch();
#print_r($records);

?>
<div>
<form action="edit_action.php">
	<div class="btn btn-secondary">
	<input value="<?php echo $records['id']; ?>" type="text" autofocus name="id"/> <br/>
	</div><br/>
	<div class="btn btn-secondary">
	<input value="<?php echo $records['name']; ?>" type="text" autofocus name="name" placeholder="Enter name"/> <br/>
	</div><br/>
	<div class="btn btn-secondary">
	<input value="<?php echo $records['age']; ?>" type="text" name="age" placeholder="Enter age"/> <br/>
	</div><br/>
	<div class="btn btn-secondary">
	<input value="<?php echo $records['city']; ?>" type="text" name="city" placeholder="Enter city"/> <br/>
	</div><br/>

	<div class="btn btn-dark"><input type="submit"/></div>
</form>
</div>
